import Vue from "vue";
import Router from "vue-router";
import HelloWorld from "@/components/HelloWorld";
import home from "@/views/home/home";
import el_select from "@/views/el_select/el_select";
import jsbarcode from "@/views/jsbarcode/jsbarcode";
import qrcode from "@/views/qrcode/qrcode";
import package_sheet from "@/views/package_sheet/package_sheet";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "HelloWorld",
      component: HelloWorld
    },
    {
      path: "/home",
      name: "home",
      component: home
    },
    {
      path: "/el_select",
      name: "el_select",
      component: el_select
    },
    {
      path: "/jsbarcode",
      name: "jsbarcode",
      component: jsbarcode
    },
    {
      path: "/qrcode",
      name: "qrcode",
      component: qrcode
    },
    {
      path: "/package_sheet",
      name: "package_sheet",
      component: package_sheet
    }
  ]
});
